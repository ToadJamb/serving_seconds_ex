defmodule ServingSeconds.Humanizer do
  alias ServingSeconds.Intervals

  @intervals Intervals.intervals()

  def humanize(seconds), do: humanize seconds, 0
  def humanize(seconds, prec) when is_integer(prec) do
    index = highest_interval_index(seconds)
    find seconds, prec, index
  end

  defp highest_interval_index(seconds)
  when seconds < 1,  do: length(@intervals) - 1

  defp highest_interval_index(seconds) do
    Intervals.intervals
    |> Enum.with_index
    |> Enum.find(fn({interval, _i}) ->
      units = Kernel.apply(Intervals, interval, [])
      seconds >= units || seconds == units
    end)
    |> fn({_units, index}) -> index end.()
  end

  defp find(seconds, prec, i, true) do
    find seconds, prec, i - 1
  end

  defp find(seconds, prec, i, false) do
    method = Enum.at(@intervals, i)
    units = Kernel.apply(Intervals, method, [])
    #candidate = (seconds / units) |> IO.inspect |> round
    candidate = calculate(seconds, units, prec)
    descriptor =
      @intervals
      |> Enum.at(i)
      |> Atom.to_string
      |> String.at(0)
    "#{candidate}#{descriptor}"
  end

  defp find(seconds, prec, i) do
    method = Enum.at(@intervals, i)
    units = Kernel.apply(Intervals, method, [])
    candidate = calculate(seconds, units, prec)
    find seconds, prec, i, keep_looking?(candidate, i)
  end

  defp keep_looking?(candidate, i) do
    #IO.puts "======================"
    #{i, candidate, Kernel.apply(Intervals, Enum.at(@intervals, i - 1), [])}
    #|> IO.inspect
    #IO.puts "======================"
    units = Kernel.apply(Intervals, Enum.at(@intervals, i), [])
    i - 1 >= 0 && (candidate * units) >= Kernel.apply(Intervals, Enum.at(@intervals, i - 1), [])
  end

  defp calculate(seconds, units, prec) do
    (seconds / units) #|> IO.inspect |> round_it(prec) |> IO.inspect
    |> round_it(prec)
  end

  defp round_it(value, 0), do: round value
  defp round_it(value, prec), do: Float.round value, prec
end
