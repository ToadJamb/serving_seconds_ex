defmodule ServingSeconds.Intervals do
  @intervals [
    :years,
    :weeks,
    :days,
    :hours,
    :minutes,
    :seconds,
  ]

  @seconds  1.0
  @minutes 60 * @seconds
  @hours   60 * @minutes
  @days    24 * @hours
  @weeks    7 * @days
  @years   52 * @weeks

  def intervals, do: @intervals

  @intervals
  |> Enum.each(fn(interval) ->
    value = Macro.escape(Module.get_attribute(__MODULE__, interval))
    def unquote(:"#{interval}")(), do: unquote(value)
  end)
end
