defmodule ServingSeconds do
  alias ServingSeconds.Humanizer
  defdelegate humanize(seconds), to: Humanizer
  defdelegate humanize(seconds, arg), to: Humanizer
end
