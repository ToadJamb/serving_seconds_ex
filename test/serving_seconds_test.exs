defmodule ServingSecondsTest do
  use ExUnit.Case
  use ExUnitProperties

  doctest ServingSeconds

  @described_module ServingSeconds

  @secs   1
  @mins  60 * @secs
  @hrs   60 * @mins
  @days  24 * @hrs
  @weeks  7 * @days
  @years 52 * @weeks

  describe ".humanize" do
    [
      {[   0],  "0s"},
      {[0.49],  "0s"},
      {[0.50],  "1s"},
      {[  59], "59s"},
      {[  60],  "1m"},

      {[@hrs - 31], "59m"},
      {[@hrs - 30], "1h"},

      {[23 * @hrs + 29 * @mins], "23h"},
      {[23 * @hrs + 30 * @mins], "1d"},

      {[6 * @days + 11 * @hrs], "6d"},
      {[6 * @days + 12 * @hrs], "1w"},

      {[51 * @weeks + 3 * @days + 11 * @hrs + 59 * @mins + 59], "51w"},
      {[365 * @days], "1y"},

      {[3650 * @days], "10y"},
      {[36500 * @days], "100y"},

      {[   0,  1],  "0.0s"},
      {[0.04,  1],  "0.0s"},
      {[0.05,  1],  "0.1s"},
      {[0.94,  1],  "0.9s"},
      {[0.951, 1],  "1.0s"},
      {[  59,  1], "59.0s"},
      {[  60,  1],  "1.0m"},
      {[  62,  1],  "1.0m"},
      {[  63,  1],  "1.1m"},


      {[1 * @hrs - 4, 1], "59.9m"},
      {[1 * @hrs - 3, 1],  "1.0h"},
      {[1 * @hrs + 2 * @mins, 1],  "1.0h"},
      {[1 * @hrs + 3 * @mins, 1],  "1.1h"},

      {[23 * @hrs + 57 * @mins, 1], "23.9h"},
      {[23 * @hrs + 58 * @mins, 1], "1.0d"},
      {[24 * @hrs + 1 * @hrs, 1], "1.0d"},
      {[24 * @hrs + 2 * @hrs, 1], "1.1d"},

      {[6 * @days + 22 * @hrs, 1], "6.9d"},
      {[6 * @days + 23 * @hrs, 1], "1.0w"},

      {[51 * @weeks + 6 * @days, 1], "51.9w"},
      {[365 * @days, 1], "1.0y"},

      {[3650 * @days, 1], "10.0y"},
      {[36500 * @days, 1], "100.3y"},
    ] |> Enum.each(fn(args = {input, expected}) ->
      params = Macro.escape(args)
      test "given #{inspect(input)}, returns #{inspect(expected)}" do
        {input, expected} = unquote(params)
        actual = Kernel.apply(@described_module, :humanize, input)
        assert actual == expected
      end
    end)
  end

  property "under 1 second" do
    check all candidate <- generator(0.0, min_seconds()) do
      assert @described_module.humanize(candidate) == "0s"
    end
  end

  property "answers in seconds" do
    check all candidate <- generator(min_seconds(), max_seconds()) do
      expected = "#{round(candidate)}s"
      assert @described_module.humanize(candidate) == expected
    end
  end

  property "answers in minutes" do
    check all candidate <- generator(max_seconds(), max_minutes()) do
      expected = "#{round(candidate / @mins)}m"
      assert @described_module.humanize(candidate) == expected
    end
  end

  property "answers in hours" do
    check all candidate <- generator(max_minutes(), max_hours()) do
      expected = "#{round(candidate / @hrs)}h"
      assert @described_module.humanize(candidate) == expected
    end
  end

  property "answers in days" do
    check all candidate <- generator(max_hours(), max_days()) do
      expected = "#{round(candidate / @days)}d"
      assert @described_module.humanize(candidate) == expected
    end
  end

  property "answers in weeks" do
    check all candidate <- generator(max_days(), max_weeks()) do
      expected = "#{round(candidate / @weeks)}w"
      assert @described_module.humanize(candidate) == expected
    end
  end

  property "answers in years" do
    check all candidate <- generator(max_weeks(), max_years()) do
      expected = "#{round(candidate / @years)}y"
      assert @described_module.humanize(candidate) == expected
    end
  end

  defp generator(min_float, max_float) do
    min_int = Float.ceil(min_float) |> round
    max_int = Float.floor(max_float) |> round

    [
      StreamData.float(min: min_float, max: max_float),
      StreamData.integer(min_int..max_int),
    ] |> StreamData.one_of
    |> StreamData.filter(fn(candidate) ->
      candidate < max_float
    end)
  end

  defp min_seconds, do:  0.5
  defp max_seconds, do: 59.5
  defp max_minutes, do: @hrs   - (@mins  / 2)
  defp max_hours,   do: @days  - (@hrs   / 2)
  defp max_days,    do: @weeks - (@days  / 2)
  defp max_weeks,   do: @years - (@weeks / 2)
  defp max_years,   do: 500 * @years / 1
end
