ServingSeconds
==============

Need to humanize seconds elapsed?

Use ServingSeconds.

Now serving up humanized seconds without all the side dishes. Delicious.

*Note*: This is an elixir port of my ruby gem by the same name.


Installation
------------

Add `serving_seconds` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:serving_seconds, "~> 0.1.0"}
  ]
end

```

Examples
========

By default, the following values are returned:

```
ServingSeconds.humanize 60          #=> '1m'
ServingSeconds.humanize 60 * 60     #=> '1h'
ServingSeconds.humanize 5 * 60 * 60 #=> '5h'
```

Precision may be specified to get decimal values:

```
ServingSeconds.humanize 0.3, 1 #=> '0.3s'
ServingSeconds.humanize 90, 1  #=> '1.5m'
ServingSeconds.humanize 90, 3  #=> '1.500m'
```


TODO
====

The long format available in the ruby gem is not yet supported.
